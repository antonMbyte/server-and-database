#include <sys/types.h>          
#include <sys/socket.h>
#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h> 
#include <stdlib.h> 

#define PATH_FILE_DATABASE "/home/anton/diplom/pc2/database/data/UserDataBase.base"
#define BUFFERMAX 1024
#define MAXLENMSG 128

typedef enum
{
	EMPTY,
	DONT_EMPTY
}rcode_t;

/*Структура хранит данные о зарегестрированных пользователей*/
typedef struct
{
	char surname[MAXLENMSG];
	char name[MAXLENMSG];
	char card_id[MAXLENMSG];
	
} initials_t;


/*Структура базы данных для одного пользователя*/
struct database_t
{
	initials_t *initials;
	struct database_t *previous;
	struct database_t *next;
		
};

/*Указатель на последнего зарегистрировавшегося посльзователя в базе*/
typedef struct database_t database_t;
database_t *global_ptr_database;

void write_dynamic_user_database(initials_t *);
void write_static_user_database(database_t *arg);
rcode_t check_status_static_user_database(FILE *);
void write_database(initials_t *); /*Обертка API*/