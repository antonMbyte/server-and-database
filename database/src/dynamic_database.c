#include </home/anton/diplom/pc2/database/inc/database.h>  /*база данных*/

/*Динамическая база данных, которая хранит информацию о пользователях(для дальнейшей аутентификации)*/
void write_dynamic_user_database(initials_t *arg)
{
	/*Данные о новых пользователях сохраняются сюда*/
	typedef struct database_t database_t;
	database_t *databases = (database_t *) malloc(sizeof(database_t));

	/*Сами данные о пользователе*/
	initials_t *data = (initials_t *) malloc(sizeof(initials_t));
	
	memcpy(data, arg, sizeof(initials_t));

	if (global_ptr_database == NULL)
	{
		databases->initials = data;
		databases->previous = NULL;
		databases->next = NULL;
		global_ptr_database = databases;
		/*Выгружаем данные о пользователи в энергонезависимую базу данных*/
		write_static_user_database(global_ptr_database);
	}
	else
	{
		databases->initials = data;
		databases->previous = global_ptr_database;
		databases->next = NULL;
		global_ptr_database->next = databases;
		global_ptr_database = databases;
		write_static_user_database(global_ptr_database);
	}
	return;
}