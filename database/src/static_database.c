#include </home/anton/diplom/pc2/database/inc/database.h>  /*база данных*/

void write_static_user_database(database_t *arg)
{
	FILE *file_fd = NULL;
	char buffer[BUFFERMAX] = {0};
	
	if ((file_fd = fopen(PATH_FILE_DATABASE, "aw")) == NULL) errors_hundler("fopen");
	
	if (EMPTY == check_status_static_user_database(file_fd))
	{
		if ((strlen(arg->initials->name) != fwrite(arg->initials->name, sizeof(char), strlen(arg->initials->name), file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(" ", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((strlen(arg->initials->surname) != fwrite(arg->initials->surname, sizeof(char), strlen(arg->initials->surname), file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(":", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(" ", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((strlen(arg->initials->card_id) != fwrite(arg->initials->card_id, sizeof(char), strlen(arg->initials->card_id), file_fd)))
			errors_hundler("fwrite");
	}
	else
	{
		if ((1 != fwrite("\n", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((strlen(arg->initials->name) != fwrite(arg->initials->name, sizeof(char), strlen(arg->initials->name), file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(" ", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((strlen(arg->initials->surname) != fwrite(arg->initials->surname, sizeof(char), strlen(arg->initials->surname), file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(":", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((1 != fwrite(" ", sizeof(char), 1, file_fd)))
			errors_hundler("fwrite");
		if ((strlen(arg->initials->card_id) != fwrite(arg->initials->card_id, sizeof(char), strlen(arg->initials->card_id), file_fd)))
			errors_hundler("fwrite");
	}

		fclose(file_fd);
	return;
}