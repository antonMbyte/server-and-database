#!/bin/bash

PATH_SERVER_LIB=/home/anton/diplom/pc2/database/lib/libdatabase.a
PATH_DATABASE_LIB=/home/anton/diplom/pc2/server/lib/libdatabase.a
PATH_DATABASE_SRC=/home/anton/diplom/pc2/database/src
PATH_SERVER_SRC=/home/anton/diplom/pc2/server/src

echo 
echo 
echo "-------------------Database---------------------------"
if [[ -f $PATH_SERVER_LIB ]]; then
	cd $PATH_DATABASE_SRC
	make clear
	cd $PATH_DATABASE_SRC
	make
fi

echo 
echo
echo "---------------------Server------------------------------"
if [[ -f $PATH_DATABASE_LIB ]]; then
	cd $PATH_SERVER_SRC
	make clear
	cd $PATH_SERVER_SRC
	make
fi