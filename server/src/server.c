/*-----------------------------------------------------------------------------------------------------------------------------------------------
The server runs in two threads:
1) User Authentication
2) Registration of a new user in the database
------------------------------------------------------------------------------------------------------------------------------------------------*/
#include </home/anton/diplom/pc2/server/inc/server.h>


int main(int argc, char const *argv[])
{
	pthread_t user_registration;
	global_ptr_database = NULL;
	//pthread_t user_authentication;
	if (0 != pthread_create(&user_registration, NULL, user_registration_hudler, NULL)) errors_hundler("pthread_create");
	//if (0 != pthread_create(&user_authentication, NULL, user_authentication_hundler, NULL)) errors_hundler("pthread_create");
	if (0 != pthread_join(user_registration, NULL)) errors_hundler("pthread_join");
	//if (0 != pthread_join(user_authentication, NULL)) errors_hundler("pthread_join"); 

	return SUCCESS;
}


void errors_hundler(char *func)
{
	if (0 == strcmp(func,"pthread_create"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"pthread_join"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"socket"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"bind"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"listen"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"accept"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"recv"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"fopen"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"fread"))
	{
		printf("%s: the condition did not degenerate (cnt_symbol != 0)\n", func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"fwrite"))
	{
		perror(func);
		exit(FAILED);
	}
	else if (0 == strcmp(func,"fseek"))
	{
		perror(func);
		exit(FAILED);
	}
}


/*Потоковый обработчик для регистрации новых клиентов (вызывается из главного потока при старте процесса)*/
void *user_registration_hudler(void *arg)
{
	char buffer[BUFFERMAX] = {0};
	struct sockaddr_in addr;
	unsigned int connection_sockfd, rw_sockfd;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT_REGISTRATION); 
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (-1 == (connection_sockfd = socket(AF_INET, SOCK_STREAM, 0))) errors_hundler("socket");
	if (-1 == bind(connection_sockfd, (struct sockaddr *)&addr, sizeof(addr))) errors_hundler("bind");
	if (-1 == listen(connection_sockfd, SOMAXCONN)) errors_hundler("listen");
	while(1)
	{
		if (-1 == (rw_sockfd = accept(connection_sockfd, 0, 0))) errors_hundler("accept");
		if (-1 == recv(rw_sockfd, buffer, sizeof(initials_t), 0)) errors_hundler("recv");

		initials_t *data = (initials_t *) buffer;
		write_database(data);
	}

	return NULL;
}
